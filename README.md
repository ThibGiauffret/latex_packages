# LaTeX Packages

This repository contains LaTeX packages I wrote to produce my documents. You will find my personal packages in the `extensions` folder. Those are not available on CTAN. 

The other folders contain the source files of the packages I publish on CTAN (*ex.* `tikz-osci`).

## Usage

For each package, you will find a documentation pdf file (either in French or English), an example and a `.sty` file.

## Reporting issues and contributing

If you encounter any issue, please report it using the [issue tracker](https://framagit.org/ThibGiauffret/latex_packages/-/issues). I'm would be glad to hear about any suggestion or improvement you may have.

**Important note:** In the issue tracker, please indicate the package concerned by the issue in the title. For example, if you want to report an issue about the `tikz-osci` package, please use the following title: `[tikz-osci] Issue title`.


## Licence

All the packages are released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt