Extensions (.sty files used to produce my documents)
-----------------------------------------------------------
Author  : Thibault Giauffret

Email   : contact at ensciences dot fr

Licence : Released under the LaTeX Project Public License v1.3c or later, see http://www.latex-project.org/lppl.txt
